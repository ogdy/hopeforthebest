import pause
from datetime import datetime, timedelta
import query

SLEEP = 60
bitstamp = open("bitstamp_" + str(SLEEP) + "s.data", 'w')
coindesk = open("coindesk_" + str(SLEEP) + "s.data", 'w')
kraken = open("kraken_" + str(SLEEP) + "s.data", 'w')

while True:
    now = datetime.now()

    pbitstamp = query.getBitcoinEuroBitstamp()
    bitstamp.write(str(pbitstamp) + "\n")
    bitstamp.flush()

    pcoindesk = query.getBitcoinEuroCoindesk()
    coindesk.write(str(pcoindesk) + "\n")
    coindesk.flush()

    pkraken = query.getBitcoinEuroKraken()
    kraken.write(str(pkraken) + "\n")
    kraken.flush()

    pause.until(now + timedelta(seconds=SLEEP))