import requests, json

def getBitcoinEuroBitstamp():
    for x in range(1, 10):
        try:
            URL = "https://www.bitstamp.net/api/v2/ticker/btceur/"
            r = requests.get(URL)
            return float(json.loads(r.text)["last"])
        except:
            print("Can't get bitcoin price ! attempt " + str(x))

def getBitcoinEuroCoindesk():
    for x in range(1, 10):
        try:
            URL = "https://api.coindesk.com/v1/bpi/currentprice.json"
            r = requests.get(URL)
            return float(json.loads(r.text)["bpi"]["EUR"]["rate_float"])
        except:
            print("Can't get bitcoin price ! attempt " + str(x))
     
def getBitcoinEuroKraken():
    for x in range(1, 10):
        try:
            URL = "https://api.kraken.com/0/public/Ticker?pair=BTCEUR"
            r = requests.get(URL)
            return float(json.loads(r.text)["result"]["XXBTZEUR"]["c"][0])
        except:
            print("Can't get bitcoin price ! attempt " + str(x))

logFile = open("data2/coindesk_60s.data", 'r')

def getBitcoinEuroPremade():
    global logFile
    return float(logFile.readline())

def initSourceFile():
    global logFile
    logFile = open("data3/coindesk_60s.data", 'r')